package com.sems.organicappsems;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.ar.core.Anchor;
import com.google.ar.core.AugmentedImage;
import com.google.ar.core.AugmentedImageDatabase;
import com.google.ar.core.Config;
import com.google.ar.core.Frame;
import com.google.ar.core.Session;
import com.google.ar.core.TrackingState;
import com.google.ar.sceneform.AnchorNode;
import com.google.ar.sceneform.FrameTime;
import com.google.ar.sceneform.collision.Box;
import com.google.ar.sceneform.math.Quaternion;
import com.google.ar.sceneform.math.Vector3;
import com.google.ar.sceneform.rendering.ModelRenderable;
import com.google.ar.sceneform.rendering.Renderable;
import com.google.ar.sceneform.ux.ArFragment;
import com.google.ar.sceneform.ux.TransformableNode;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;

public class MainActivity extends AppCompatActivity {

    private CustomArFragment arFragment;
    private boolean shouldAddModel = true;
    private Button btnRotate;
    private Button btnTranslate;
    private TransformableNode node;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        arFragment = (CustomArFragment) getSupportFragmentManager().findFragmentById(R.id.sceneform_fragment);
        arFragment.getPlaneDiscoveryController().hide();
        arFragment.getArSceneView().getScene().addOnUpdateListener(this::onUpdateFrame);

        btnTranslate = findViewById(R.id.btnTranslate);
        btnRotate = findViewById(R.id.btnRotate);

        btnTranslate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("btnTranslate","translate");
                if(node != null){
                    node.setLocalPosition(new Vector3(0, 0, 0));
                    node.select();
                }
            }
        });

        btnRotate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("btnRotate","rotate");
                if(node != null){
                    node.setLocalRotation(Quaternion.axisAngle(new Vector3(1f, 0, 0), -90f));
                    node.select();
                }
            }
        });
    }

    public boolean setupAugmentedImageDb(Config config, Session session){
        AugmentedImageDatabase augmentedImageDatabase;

        Bitmap bitmap = loadAugmentedImage();
        Bitmap bitmap2 = loadAugmentedImage2();

        if (bitmap == null){
            return  false;
        }

        augmentedImageDatabase = new AugmentedImageDatabase(session);
        augmentedImageDatabase.addImage("tie", bitmap);
        augmentedImageDatabase.addImage("airplane", bitmap2);

        config.setAugmentedImageDatabase(augmentedImageDatabase);
        return true;
    }

    private Bitmap loadAugmentedImage(){
        try (InputStream is = getAssets().open("tie_fighter/tie.jpg")){
            return BitmapFactory.decodeStream(is);
        }
        catch (IOException e){
            Log.e("ImageLoad", "IO Exception while loading", e);
        }
        return null;
    }

    private Bitmap loadAugmentedImage2(){
        try (InputStream is = getAssets().open("airplane/airplane.jpg")){
            return BitmapFactory.decodeStream(is);
        }
        catch (IOException e){
            Log.e("ImageLoad", "IO Exception while loading", e);
        }
        return null;
    }

    private void onUpdateFrame(FrameTime frameTime){
        Frame frame = arFragment.getArSceneView().getArFrame();

        Collection<AugmentedImage> augmentedImages = frame.getUpdatedTrackables(AugmentedImage.class);

        for (AugmentedImage augmentedImage : augmentedImages){
            if (augmentedImage.getTrackingState() == TrackingState.TRACKING){
                Log.d("onUpdateFrame","Tracking " + augmentedImage.getName());

                if (augmentedImage.getName().equals("tie") && shouldAddModel) {
                    placeObject(arFragment,
                            augmentedImage.createAnchor(augmentedImage.getCenterPose()),
                            Uri.parse("tie_fighter/tie_fighter.sfb"));
                    shouldAddModel = false;
                }
                if(augmentedImage.getName().equals("airplane") && shouldAddModel) {
                    placeObject(arFragment,
                            augmentedImage.createAnchor(augmentedImage.getCenterPose()),
                            Uri.parse("airplane/Airplane.sfb"));
                    shouldAddModel = false;
                }
            }
        }
    }

    private void placeObject(ArFragment fragment, Anchor anchor, Uri model){
        Log.e("placeObject", "placing object");

        ModelRenderable.builder()
                .setSource(fragment.getContext(), model)
                .build()
                .thenAccept(renderable -> addNodeToScene(fragment, anchor, renderable))
                .exceptionally((throwable -> {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setMessage(throwable.getMessage())
                            .setTitle("Error!");
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    return null;
                }));

    }

    private void addNodeToScene(ArFragment fragment, Anchor anchor, Renderable renderable) {
        if (renderable == null) {
            Log.d("TAGss", "Renderable not yet ready");
            return;
        }
        AnchorNode anchorNode = new AnchorNode(anchor);
        node = new TransformableNode(fragment.getTransformationSystem());
        node.setRenderable(renderable);

        node.getScaleController().setMaxScale(0.2f);
        node.getScaleController().setMinScale(0.1f);

        node.setLocalPosition(new Vector3(0, 0, 0));
        node.setParent(anchorNode);

        fragment.getArSceneView().getScene().addChild(anchorNode);
        node.select();
        Log.d("addNodeToScene", "add node to scene");
    }
}
